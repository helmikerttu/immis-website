# Immis Website

This project is a website for my visionary and experimental software company Immis.
The company doesn't do anything yet, but is there for my future inspirations.
The page can be found at [immis.fi](https://www.immis.fi/).

## How to run

To run this app locally, you must first install [Node.js](https://nodejs.org/en/).
Then you can start the app with `npm start` command in the `frontend` folder.
